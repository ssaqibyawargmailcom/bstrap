Bootstrap has made the life of back-end developers a little easier. With the ability to make almost all the apps look cleaner then how they would originally appear, it has indeed been a great tool.

As I began using it for the first time, I decided to create a skeleton project I can use as a reference and basis for future project.

For details of the tutorial navigate to http://www.syawar.com/2014/10/04/bootstrap-django-templates-and-admin-a-complete-step-by-step/